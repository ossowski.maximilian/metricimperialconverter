package pl.maxossowski.converter;

import java.math.BigDecimal;
import java.math.RoundingMode;

class ImperialToMetricConverter {

    private static final double METERS = 0.3048;
    private static final double CENTIMETERS = 2.54;
    private static final double LITERS = 3.7854;
    private static final double KILOGRAMS = 0.4535;
    private static final double CELSIUS = 5.0 / 9;
    private static final double MAX_PRECISION_NUMBER = 999999999999999.;
    private static final double ABSOLUTE_ZERO = -459.67;
    private static final int ROUNDING_NUMBER = 2;

     static double footsToMeter(double value) {
        if (value < 0.0)
            throw new IllegalArgumentException("Value less then zero!");
        if (value > MAX_PRECISION_NUMBER / METERS)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

        return round(value * METERS, ROUNDING_NUMBER);
    }

    static double inchesToCentimeter(double value) {
        if (value < 0.0)
            throw new IllegalArgumentException("Value less then zero!");
        if (value > MAX_PRECISION_NUMBER / CENTIMETERS)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

        return round(value * CENTIMETERS, ROUNDING_NUMBER);
    }

    static double gallonsToLiters(double value) {
        if (value < 0.0)
            throw new IllegalArgumentException("Value less then zero!");
        if (value > MAX_PRECISION_NUMBER / LITERS)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

        return  round(value * LITERS, ROUNDING_NUMBER);
    }

    static double poundsToKilograms(double value) {
        if (value < 0.0)
            throw new IllegalArgumentException("Value less then zero!");
        if (value > MAX_PRECISION_NUMBER / KILOGRAMS)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

        return round(value * KILOGRAMS, ROUNDING_NUMBER);
    }

    static double fahrenheitsToCelsius(double value) {
        if (value < ABSOLUTE_ZERO)
            throw new IllegalArgumentException("Value less then zero!");
        if (value > MAX_PRECISION_NUMBER / CELSIUS)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

        return round ((value - 32) * CELSIUS, ROUNDING_NUMBER);
    }

    private static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
