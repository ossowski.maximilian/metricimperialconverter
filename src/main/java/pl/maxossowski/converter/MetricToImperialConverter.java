package pl.maxossowski.converter;

import java.math.BigDecimal;
import java.math.RoundingMode;

class MetricToImperialConverter {

    private static final double FOOTS = 3.2808;
    private static final double INCHES = 0.3937;
    private static final double GALLONS = 0.2641;
    private static final double POUNDS = 2.2046;
    private static final double FAHRENHEITS = 9.0 / 5;
    private static final double MAX_PRECISION_NUMBER = 999999999999999.;
    private static final double ABSOLUTE_ZERO = -273.15;
    private static final int ROUNDING_NUMBER = 2;

    static double metersToFoots(double value) {

        if (value < 0.0)
            throw new IllegalArgumentException("Value less then zero!");
        if (value > MAX_PRECISION_NUMBER / FOOTS)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

        return round(value * FOOTS, ROUNDING_NUMBER);

    }

    static double centimeterToInches(double value) {
        if (value < 0.0)
            throw new IllegalArgumentException("Value less then zero!");
        if (value > MAX_PRECISION_NUMBER / INCHES)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

        return round(value * INCHES, ROUNDING_NUMBER);

    }

    static double litersToGallons(double value) {
        if (value < 0.0)
            throw new IllegalArgumentException("Value less then zero!");
        if(value > MAX_PRECISION_NUMBER / GALLONS)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

        return round(value * GALLONS, ROUNDING_NUMBER);

    }

    static double kilogramsToPounds(double value) {
        if (value < 0.0)
            throw new IllegalArgumentException("Value less then zero!");
        if(value > MAX_PRECISION_NUMBER / POUNDS)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

        return round(value * POUNDS, ROUNDING_NUMBER);

    }

    static double celsiusToFahrenheits(double value) {
        if (value < ABSOLUTE_ZERO)
            throw new IllegalArgumentException("Value less then absolute zero!");
        if (value > MAX_PRECISION_NUMBER / FAHRENHEITS)
            throw new IllegalArgumentException("Argument to big! Precision will be lost!");

            return round(value * FAHRENHEITS + 32, ROUNDING_NUMBER);
    }

    private static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
