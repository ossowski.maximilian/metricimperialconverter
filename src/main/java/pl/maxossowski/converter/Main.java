package pl.maxossowski.converter;

public class Main {

    public static void main(String[] args) {
        // Some simple output to test jar file execution

        System.out.println("400 meter is : " + MetricToImperialConverter.metersToFoots(400.0) + " foots");
        System.out.println("24.5 centimeter is : " + MetricToImperialConverter.centimeterToInches(24.5) + " inches");
        System.out.println("58.9 liters is : " + MetricToImperialConverter.litersToGallons(58.9) + " gallons");
        System.out.println("320 kilograms is : " + MetricToImperialConverter.kilogramsToPounds(320.0) + " pounds");
        System.out.println("34 celsius is : " + MetricToImperialConverter.celsiusToFahrenheits(34.0) + " fahrenheit");

        System.out.println(" ");

        System.out.println("1450 foots is : " + ImperialToMetricConverter.footsToMeter(1450.0) + " meters");
        System.out.println("99.3 inches is : " + ImperialToMetricConverter.inchesToCentimeter(99.3) + " centimeters");
        System.out.println("23.83 gallons is : " + ImperialToMetricConverter.gallonsToLiters(23.83) + " liters");
        System.out.println("90.7 pounds is : " + ImperialToMetricConverter.poundsToKilograms(90.7) + " kilograms");
        System.out.println("-234.5 fahrenheit is : " + ImperialToMetricConverter.fahrenheitsToCelsius(-234.5) + " celsius");
    }
}
