package pl.maxossowski.converter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

class MetricToImperialTest {

    @Test
    void givenCorrectInput_meterToFoots_shouldReturnCorrectAnswer() {
        double answer = MetricToImperialConverter.metersToFoots(123.5);

        Assertions.assertEquals(405.18, answer);
    }

    @Test
    void givenMinusInput_meterToFoots_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.metersToFoots(-20.00));
    }

    @Test
    void givenZeroValue_meterToFoots_shouldReturnZero() {
        double answer = MetricToImperialConverter.metersToFoots(0.0);

        Assertions.assertEquals(0.00, answer);
    }

    @Test
    void givenVeryLargeNumber_meterToFoots_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.metersToFoots(12312312322278461.5465));
    }

    @Test
    void givenVerySmallNumber_meterToFoots_shouldReturnZero() {
        double answer = MetricToImperialConverter.metersToFoots(Double.MIN_VALUE);

        Assertions.assertEquals(0.0, answer);
    }

    // ------------------------------------------------------------------------------

    @Test
    void givenCorrectInput_centimetersToInches_shouldReturnCorrectAnswer() {
        double answer = MetricToImperialConverter.centimeterToInches(22.0);

        Assertions.assertEquals(8.66, answer);
    }

    @Test
    void givenMinusInput_centimetersToInches_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.centimeterToInches(-20.00));
    }

    @Test
    void givenZeroValue_centimetersToInches_shouldReturnZero() {
        double answer = MetricToImperialConverter.centimeterToInches(0.0);

        Assertions.assertEquals(0.0, answer);
    }

    @Test
    void givenVeryLargeNumber_centimetersToInches_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.centimeterToInches(12312312322278461.5465));
    }

    @Test
    void givenVerySmallNumber_centimetersToInches_shouldReturnZero() {
        double answer = MetricToImperialConverter.centimeterToInches(Double.MIN_VALUE);

        Assertions.assertEquals(0.0, answer);
    }

    // ------------------------------------------------------------------------------

    @Test
    void givenCorrectInput_literToGallons_shouldReturnCorrectAnswer() {
        double answer = MetricToImperialConverter.litersToGallons(22.0);

        Assertions.assertEquals(5.81, answer);
    }

    @Test
    void givenMinusInput_literToGallons_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> MetricToImperialConverter.litersToGallons(-20.00));
    }

    @Test
    void givenZeroValue_literToGallons_shouldReturnZero() {
        double answer = MetricToImperialConverter.litersToGallons(0.0);

        Assertions.assertEquals(0.0, answer);
    }

    @Test
    void givenVeryLargeNumber_literToGallons_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.litersToGallons(12312312322278461.5465));
    }

    @Test
    void givenVerySmallNumber_literToGallons_shouldReturnZero() {
        double answer = MetricToImperialConverter.litersToGallons(Double.MIN_VALUE);

        Assertions.assertEquals(0.0, answer);
    }

    // ------------------------------------------------------------------------------

    @Test
    void givenCorrectInput_kilogramsToPounds_shouldReturnCorrectAnswer() {
        double answer = MetricToImperialConverter.kilogramsToPounds(22.0);

        Assertions.assertEquals(48.5, answer);
    }

    @Test
    void givenMinusInput_kilogramsToPounds_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> MetricToImperialConverter.kilogramsToPounds(-20.00));
    }

    @Test
    void givenZeroValue_kilogramsToPounds_shouldReturnZero() {
        double answer = MetricToImperialConverter.kilogramsToPounds(0.0);

        Assertions.assertEquals(0.0, answer);
    }

    @Test
    void givenVeryLargeNumber_kilogramsToPounds_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.kilogramsToPounds(12312312322278461.5465));
    }

    @Test
    void givenVerySmallNumber_kilogramsToPounds_shouldReturnZero() {
        double answer = MetricToImperialConverter.kilogramsToPounds(Double.MIN_VALUE);

        Assertions.assertEquals(0.0, answer);
    }

    // ------------------------------------------------------------------------------

    @Test
    void givenCorrectInput_celsiusToFahrenheits_shouldReturnCorrectAnswer() {
        double answer = MetricToImperialConverter.celsiusToFahrenheits(22.0);

        Assertions.assertEquals(71.6, answer);
    }

    @Test
    void givenInputBelowAbsoluteZero_celsiusToFahrenheits_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> MetricToImperialConverter.celsiusToFahrenheits(-290.20));
    }

    @Test
    void givenZeroValue_celsiusToFahrenheits_shouldReturnCorrectAnswer() {
        double answer = MetricToImperialConverter.celsiusToFahrenheits(0.0);

        Assertions.assertEquals(32.0, answer);
    }

    @Test
    void givenVeryLargeNumber_celsiusToFahrenheits_shouldThrowException() {

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> MetricToImperialConverter.celsiusToFahrenheits(12312312322278461.5465));
    }

    @Test
    void givenVerySmallNumber_celsiusToFahrenheits_shouldReturnCorrectAnswer() {
        double answer = MetricToImperialConverter.celsiusToFahrenheits(Double.MIN_VALUE);

        Assertions.assertEquals(32.0, answer);
    }
}
