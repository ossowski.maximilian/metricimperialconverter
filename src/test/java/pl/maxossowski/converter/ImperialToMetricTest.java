package pl.maxossowski.converter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class ImperialToMetricTest {

    @Test
    void givenCorrectInput_footsToMeter_shouldReturnCorrectAnswer(){
        double answer = ImperialToMetricConverter.footsToMeter(243.231);
        Assertions.assertEquals(74.14, answer);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-20.0, 12312312322278461.5465})
    void givenInputDoubles_footsToMeter_shouldThrowException(double doubles){
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.footsToMeter(doubles));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.0, Double.MIN_VALUE})
    void givenInputDoubles_footsToMeter_shouldReturnZero(double doubles){
        Assertions.assertEquals(0.0, ImperialToMetricConverter.footsToMeter(doubles));
    }

//---------------------------------------------------------------------------------------------------
    @Test
    void givenCorrectInput_inchesToCentimeter_shouldReturnCorrectAnswer(){
        double answer = ImperialToMetricConverter.inchesToCentimeter(243.231);
        Assertions.assertEquals(617.81, answer);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-20.0, 12312312322278461.5465})
    void givenInputDoubles_inchesToCentimeter_shouldThrowException(double doubles){
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.inchesToCentimeter(doubles));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.0, Double.MIN_VALUE})
    void givenInputDoubles_inchesToCentimeter_shouldReturnZero(double doubles){
        Assertions.assertEquals(0.0, ImperialToMetricConverter.inchesToCentimeter(doubles));
    }

    //---------------------------------------------------------------------------------------------------
    @Test
    void givenCorrectInput_gallonsToLiters_shouldReturnCorrectAnswer(){
        double answer = ImperialToMetricConverter.gallonsToLiters(243.231);
        Assertions.assertEquals(920.73, answer);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-20.0, 12312312322278461.5465})
    void givenInputDoubles_gallonsToLiters_shouldThrowException(double doubles){
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.gallonsToLiters(doubles));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.0, Double.MIN_VALUE})
    void givenInputDoubles_gallonsToLiters_shouldReturnZero(double doubles){
        Assertions.assertEquals(0.0, ImperialToMetricConverter.gallonsToLiters(doubles));
    }

    //---------------------------------------------------------------------------------------------------
    @Test
    void givenCorrectInput_poundsToKilograms_shouldReturnCorrectAnswer(){
        double answer = ImperialToMetricConverter.poundsToKilograms(243.231);
        Assertions.assertEquals(110.31, answer);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-20.0, 12312312322278461.5465})
    void givenInputDoubles_poundsToKilograms_shouldThrowException(double doubles){
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.poundsToKilograms(doubles));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.0, Double.MIN_VALUE})
    void givenInputDoubles_poundsToKilograms_shouldReturnZero(double doubles){
        Assertions.assertEquals(0.0, ImperialToMetricConverter.poundsToKilograms(doubles));
    }

    //---------------------------------------------------------------------------------------------------

    @Test
    void givenCorrectInput_fahrenheitsToCelsius_shouldReturnCorrectAnswer(){
        double answer = ImperialToMetricConverter.fahrenheitsToCelsius(243.231);
        Assertions.assertEquals(117.35, answer);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-520.0, 12312312322278461.5465})
    void givenInputDoubles_fahrenheitsToCelsius_shouldThrowException(double doubles){
        Assertions.assertThrows(IllegalArgumentException.class, () -> ImperialToMetricConverter.fahrenheitsToCelsius(doubles));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.0, Double.MIN_VALUE})
    void givenInputDoubles_fahrenheitsToCelsius_shouldReturn1777(double doubles){
        Assertions.assertEquals(-17.78, ImperialToMetricConverter.fahrenheitsToCelsius(doubles));
    }

}
