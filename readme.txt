Metric To Imperial Converter

Specification:

- Write 2 classes, which will convert values from metric system to imperial system and vice versa
- One class has to convert values from metric to imperial values
- The other class should convert from imperial to metric values
- The classes should convert following values:
    - Meter vs Feet
    - Centimeter vs Inches
    - Liter vs Gallons
    - Kilogram vs Pound
    - Celsius vs Fahrenheit
- The precision should be no more then 0.01
- Use Maven
- Project should implement the standard Maven structure
- Project should get through the hole maven cycle
- The compilation should result in an JAR file, which can be run from console
- Implement unit test using the JUnit library
    - Each method should have at least 5 test cases
    - Use maven to put the dependencies in your project
- The maven test phase should run your tests
- Put this project in your repository in  GitLab.
- Add at least 5 commits.
    - Each commit should have an description
    - Create a merge request to the master branch

Architecture:
 - basic console Application

Technologies/Tools:
- Maven
- Git
- Unit tests (JUnit)